import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

import {
  Form,
  Row,
  Col,
  InputGroup,
  FormControl,
  Button,
} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../style/global.css";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [passwordErr, setPasswordErr] = useState("");
  const [emailErr, setEmailErr] = useState("");

  const navigate = useNavigate();

  const validation = () => {
    let validate = true;

    if (email === "" || email === "undefined") {
      setEmailErr("Email is required");
      validate = false;
    }
    if (password === "" || password === "undefined") {
      setPasswordErr("Password is required");
      validate = false;
    }
    return validate;
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (validation()) {
    }
  };

  const handleLogin = () => {
    if (validation()) {
      navigate("/boards");
    }
  };

  return (
    <div className="container">
      <div className="border">
        <Form onSubmit={handleSubmit}>
          <Row className="align-items-center">
            <h2>LOGIN</h2>
            <div className="fields">
              <Col xs="auto">
                <Form.Control
                  className="mb"
                  id="inlineFormInput"
                  placeholder="Email Address"
                  value={email}
                  onChange={(e) => (setEmail(e.target.value), setEmailErr(""))}
                />
                <div className="errors">{emailErr ? emailErr : ""}</div>
              </Col>
            </div>

            <div className="fields">
              <Col xs="auto">
                <Form.Label htmlFor="inlineFormInputGroup" visuallyHidden>
                  Password
                </Form.Label>
                <InputGroup className="mb">
                  <FormControl
                    id="inlineFormInputGroup"
                    placeholder="Password"
                    onChange={(e) => (
                      setPassword(e.target.value), setPasswordErr("")
                    )}
                  />
                </InputGroup>
                <div className="errors">{passwordErr ? passwordErr : ""}</div>
              </Col>
            </div>

            <div className="fields">
              <Col xs="auto">
                <Button type="submit" className="mb" onClick={handleLogin}>
                  LOGIN
                </Button>
              </Col>
            </div>
          </Row>
        </Form>
      </div>
    </div>
  );
};

export default Login;
