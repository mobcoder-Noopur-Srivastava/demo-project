import axios from 'axios';
import API from '../config/api';
//authorization
axios.defaults.headers.common['Authorization'] = `${API.AUTH}`;


export const userLoginService = params => axios.post(API.USER_LOGIN, params);
export const userSignUpService = params => axios.post(API.USER_SIGNUP, params);
