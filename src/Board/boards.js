import React, { useState } from "react";
import { Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

import { Modal, Card} from "react-bootstrap";

const Boards = () => {
  const [show, setShow] = useState(false);
  const navigate = useNavigate();

  const handleModal = () => {
    setShow(true);
  };

  const handleClose = () => {
    setShow(false);
  };

  const handleBoard = () =>{
    navigate('/boards/Details');

  }

  
  
  return (
    <div>
      <Modal show={show} onHide={handleClose} >
        <Modal.Header  closeButton>
          <Modal.Title className="create-new">Create New Board</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <input type="text" placeholder="Board Name" className="board-input" />
          <div className="d-flex mt-5">
            <button className="btn btn-success w-50" id="create">Create</button>
          </div>
        </Modal.Body>
      </Modal>
      <div>
        <h2 id="my-boards">My Boards</h2>
       
        <div className="create-board-btn">
          <Button variant="success" onClick={handleModal}>
            Create New Board
          </Button>{" "}
        </div>
        <div >
        <div className="card">
          <Card
            style={{
              width: "10rem",
              height: "4rem",
              backgroundColor: "purple",
              color: "white",
            }}
          >
            <Card.Body>
              <Card.Subtitle className="mb-2 text-muted"></Card.Subtitle>
              <Card.Text onClick={handleBoard}>Board 1</Card.Text>
            </Card.Body>
          </Card>
        </div>
        <div className="card">
          <Card
            style={{
              width: "10rem",
              height: "4rem",
              backgroundColor: "#E75480",
              color: "white",
            }}
          >
            <Card.Body>
              <Card.Subtitle className="mb-2 text-muted"></Card.Subtitle>
              <Card.Text onClick={handleBoard}>Board 2</Card.Text>
            </Card.Body>
          </Card>
          </div>
       
        </div>
      </div>
      </div>
  );
};

export default Boards;
