import React from "react";

const BoardDetails = () => {
  return (
      <div>
          <h2>Board Details</h2>
    <div className="buttons">
      <span className="bootstrap-btns">
        <button type="button" class="btn btn-secondary">
          To Do
        </button>
      </span>
      <span className="bootstrap-btns">
        <button type="button" class="btn btn-secondary">
          In Progress
        </button>
      </span>

      <span className="bootstrap-btns">
        <button type="button" class="btn btn-secondary">
          On Hold
        </button>
      </span>
      <span className="bootstrap-btns">
        <button type="button" class="btn btn-secondary">
          Completed
        </button>
      </span>
      <span className="bootstrap-btns">
        <button type="button" class="btn btn-secondary">
          Released
        </button>
      </span>
    </div>
    <div className="buttons">
    <span className="bootstrap-btns">

        <button type="button" class="btn btn-secondary">
          Task 1
        </button>
        </span>

    </div>
    </div>
  );
};

export default BoardDetails;
