import { development } from './env';

export default (() => {
  switch (process.env.REACT_APP_ENV) {

    case 'dev':
    case 'dev ':
    case 'development':
      return development;

  
    default:
      return development;
  }
})()