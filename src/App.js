import { Routes, Route } from "react-router-dom";
import "./App.css";

import Login from "./Auth/LogIn/login";
import SignUp from "./Auth/SignUp/signUp";
import Boards from "./Board/boards";
import BoardDetails from "./Board/BoardDetails/boardDetails";


function App() {
  return (
    <div>
      <Routes>
        <Route path="/" element={<SignUp />} />
        <Route exact path="/login" element={<Login/>} />
        <Route exact path="/boards" element={<Boards/>} />
        <Route exact path="/boards/details" element={<BoardDetails/>} />


    </Routes>
    </div>
  );
}

export default App;
